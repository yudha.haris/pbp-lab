import 'package:flutter/cupertino.dart';
import 'package:lab_6/core/models/note.dart';

class NoteState with ChangeNotifier {
  List<Note> notes = [];

  List<Note> Notes() => notes;

  addNote(String senderText, String bodyText) {
    notes.add(Note(sender: senderText, body: bodyText));
    notifyListeners();
  }

  deleteNote(int index) {
    notes.removeAt(index);
    notifyListeners();
  }
}
