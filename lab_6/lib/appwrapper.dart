import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/presentation/sceens/_screens.dart';
import 'package:provider/provider.dart';

import 'core/states/color_state.dart';
import 'core/states/note_state.dart';

class AppWrapper extends StatefulWidget {
  @override
  State<AppWrapper> createState() => _AppWrapperState();
}

class _AppWrapperState extends State<AppWrapper> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<NoteState>(create: (context) => NoteState()),
        ChangeNotifierProvider<ColorState>(create: (context) => ColorState()),
      ],
      child: Consumer<ColorState>(builder: (context, colorState, _) {
        return MaterialApp(
          title: "Lab 6",
          theme: ThemeData(
              hoverColor: Color(0xFFD9E89E),
              cardColor: Color(0xFF4C6245),
              bottomAppBarColor: const Color(0xFF8DB580)),
          home: MainPage(),
        );
      }),
    );
  }
}
