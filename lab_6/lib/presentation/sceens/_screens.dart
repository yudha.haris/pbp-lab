import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/core/constants/custom_size.dart';
import 'package:lab_6/core/constants/dummy_database.dart';
import 'package:lab_6/core/states/note_state.dart';
import 'package:lab_6/presentation/widgets/_widgets.dart';
import 'package:provider/provider.dart';

part 'mainpage.dart';
