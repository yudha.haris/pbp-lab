part of '_screens.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  TextEditingController _noteController = TextEditingController();
  TextEditingController _titleController = TextEditingController();
  bool _showComments = true;
  bool showTopModals = false;
  bool _isLogin = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).bottomAppBarColor,
        elevation: 0,
        title: Container(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: Image.asset(
            'assets/image/logo_tc.png',
            height: MediaQuery.of(context).size.height / 20,
            fit: BoxFit.scaleDown,
          ),
        ),
        actions: [
          InkWell(
            onTap: () {
              setState(() {
                showTopModals = !showTopModals;
              });
            },
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), border: Border.all(color: Colors.white)),
              margin: EdgeInsets.only(right: 20, top: 10, bottom: 10),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: const Icon(
                Icons.menu,
                color: Colors.white,
              ),
            ),
          )
        ],
        automaticallyImplyLeading: false,
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 20, bottom: 30),
                  child: const FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Text(
                      'Share Your Stories!',
                      style: TextStyle(fontWeight: FontWeight.w900, fontSize: 30),
                    ),
                  ),
                ),
                Container(
                  width: CustomSize.maxWidth(context) * 0.9,
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  decoration: BoxDecoration(
                    color: Theme.of(context).cardColor,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          DummyDatabase.textTitle,
                          style: TextStyle(
                              color: Theme.of(context).hoverColor,
                              fontSize: 25,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, bottom: 20),
                        alignment: Alignment.centerLeft,
                        child: const Text(
                          'by ' + DummyDatabase.textSender,
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        child: const Text(
                          DummyDatabase.textBody,
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 30,
                ),
                if (_showComments)
                  Consumer<NoteState>(builder: (context, noteProvider, _) {
                    return CustomListView(items: noteProvider.notes);
                  }),
                if (_isLogin)
                  Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(bottom: 0, top: 20, left: 20, right: 20),
                        child: Consumer<NoteState>(builder: (context, noteProvider, _) {
                          return ElevatedButton(
                            onPressed: () {
                              setState(() {
                                _showComments = !_showComments;
                              });
                            },
                            child: Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                (_showComments) ? 'Hide Comments' : 'Show Comments',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                                side: BorderSide(
                                  width: 1.0,
                                  color: Theme.of(context).bottomAppBarColor,
                                ),
                                primary: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10))),
                          );
                        }),
                      ),
                      Container(
                        width: CustomSize.maxWidth(context) / 1.1,
                        margin: const EdgeInsets.only(bottom: 2, top: 4),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(const Radius.circular(10)),
                            border:
                                Border.all(color: Theme.of(context).bottomAppBarColor, width: 2)),
                        child: TextField(
                          maxLines: null,
                          textInputAction: TextInputAction.done,
                          controller: _titleController,
                          decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
                              hintText: 'Write Name'),
                        ),
                      ),
                      Container(
                        width: CustomSize.maxWidth(context) / 1.1,
                        height: CustomSize.maxHeight(context) / 4,
                        margin: const EdgeInsets.only(bottom: 16, top: 8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(const Radius.circular(10)),
                            border:
                                Border.all(color: Theme.of(context).bottomAppBarColor, width: 2)),
                        child: TextField(
                          maxLines: null,
                          textInputAction: TextInputAction.done,
                          controller: _noteController,
                          decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
                              hintText: 'Write a comment...'),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
                        child: Consumer<NoteState>(builder: (context, noteProvider, _) {
                          return ElevatedButton(
                            onPressed: () {
                              if (_noteController.text.isNotEmpty &&
                                  _titleController.text.isNotEmpty) {
                                setState(() {
                                  noteProvider.addNote(_titleController.text, _noteController.text);
                                  _titleController.text = "";
                                  _noteController.text = "";
                                });
                              }
                            },
                            child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                child: Text('Add Comment')),
                            style: ElevatedButton.styleFrom(
                                primary: Theme.of(context).cardColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10))),
                          );
                        }),
                      ),
                      Container(
                        color: Theme.of(context).bottomAppBarColor,
                        alignment: Alignment.center,
                        padding: const EdgeInsets.only(top: 15, right: 10, left: 10, bottom: 20),
                        width: CustomSize.maxWidth(context),
                        child: const Text(
                          'Copyright © 2021 PBP D07 University of Indonesia',
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                if (!_isLogin)
                  Container(
                    padding: const EdgeInsets.only(top: 20, bottom: 30),
                    child: FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Text(
                        'Login to add comment',
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: Theme.of(context).cardColor),
                      ),
                    ),
                  ),
              ],
            ),
            AnimatedContainer(
                decoration: BoxDecoration(
                    color: Theme.of(context).bottomAppBarColor,
                    borderRadius: BorderRadius.vertical(bottom: Radius.circular(10))),
                height: (showTopModals) ? CustomSize.maxHeight(context) / 3.5 : 0,
                duration: const Duration(milliseconds: 300),
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: const Text(
                          'Informasi Obat',
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: const Text(
                          'Pengalaman',
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: const Text(
                          'WashYourLyrics',
                          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _isLogin = !_isLogin;
                              showTopModals = !showTopModals;
                            });
                          },
                          child: Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              child: Text((!_isLogin) ? 'Login' : 'Logout')),
                          style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).cardColor,
                              shape:
                                  RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
                        ),
                      ),
                      if (!_isLogin)
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                          child: ElevatedButton(
                            onPressed: () {},
                            child: Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                child: Text('Sign Up')),
                            style: ElevatedButton.styleFrom(
                                primary: Theme.of(context).cardColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10))),
                          ),
                        ),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
