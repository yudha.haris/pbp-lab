part of '_widgets.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: CustomSize.maxWidth(context) / 1.5,
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.only(top: CustomSize.maxHeight(context) / 6),
      child: Column(
        children: [
          FittedBox(
              fit: BoxFit.fitWidth,
              child: Icon(
                Icons.account_circle_sharp,
                color: Colors.blueGrey,
                size: CustomSize.maxWidth(context) / 3,
              )),
          const FittedBox(
            fit: BoxFit.fitWidth,
            child: Text(
              'Hello user!',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          ),
          Consumer<ColorState>(builder: (context, colorState, _) {
            return Switch(
              value: colorState.darkMode,
              onChanged: (value) {
                if (value) {
                  colorState.turnOnDarkMode();
                } else {
                  colorState.turnOffDarkMode();
                }
              },
            );
          })
        ],
      ),
    );
  }
}
