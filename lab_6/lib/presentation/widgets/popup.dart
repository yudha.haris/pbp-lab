part of '_widgets.dart';

class CustomDialogLab extends StatelessWidget {
  int index;
  CustomDialogLab({required this.index});

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: const Icon(
                  Icons.warning,
                  color: Colors.red,
                  size: 35,
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 5),
                width: CustomSize.maxWidth(context) / 2,
                alignment: Alignment.center,
                child: const Text(
                  'Hapus Note?',
                  textAlign: TextAlign.center,
                ),
              ),
              Consumer<NoteState>(builder: (context, noteProvider, _) {
                return ElevatedButton(
                  key: Key('popupOkeButton'),
                  onPressed: () {
                    //  _notes.removeAt(index);
                    noteProvider.deleteNote(index);
                    Navigator.pop(context);
                  },
                  child: const Text(
                    'Hapus',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.only(top: 10, bottom: 10, left: 50, right: 50),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      )),
                );
              })
            ],
          ),
        ));
  }
}
