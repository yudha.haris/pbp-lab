import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_6/core/constants/custom_size.dart';
import 'package:lab_6/core/states/color_state.dart';
import 'package:lab_6/core/states/note_state.dart';
import 'package:provider/provider.dart';

part 'custom_listview.dart';
part 'main_drawer.dart';
part 'popup.dart';
