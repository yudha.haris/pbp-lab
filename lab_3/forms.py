from django import forms
from django.forms import widgets
from .models import Friend

# GeeksForGeeks
class DateInput(forms.DateInput):
    input_type = "date"

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets = {"dob":DateInput()}
