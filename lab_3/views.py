from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required


# Create your views here.
# Membuat index func seperti pada lab1 dan lab2

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)



@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)

    # validasi apakah form valid dan request methodnya POST
    if (form.is_valid() and request.method=="POST"):
        form.save()

        # kembali ke halaman /lab_3 setelah berhasil save
        return HttpResponseRedirect('/lab-3')

    return render(request, 'lab3_form.html', {'form':form})
