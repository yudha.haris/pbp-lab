# LAB 2

## Apakah perbedaan antara JSON dengan XML?

Meskipun sama-sama digunakan untuk merepresentasikan data, JSON dan XML memiliki perbedaan sebagai berikut:

JSON                                   |   XML                              |
---------------------------------------|------------------------------------|
|   JavaScript Object Notation         |   eXtensible Markup Language       |
|   Data direpresentasikan dalam bentuk key and value |    Data direpresentasikan oleh *markup language* dengan struktur tag|
|   Tidak menggunakan tag              |   Wajib menggunakan tag            |
|   Tidak bisa menyertakan komentar    |   Bisa diselipkan komentar         |
|   Hanya mendukung UTF-8 encoding     |   Mendukung banyak encoding        |
|   Kode untuk membaca dan membuat JSON terdapat banyak di banyak bahasa pemrograman karena hanya berbentuk text|  Perlu menulis program untuk mengolah, mengirim, dan menerima XML karena hanya berupa informasi yang dibungkus tag |

## Apakah perbedaan antara HTML dengan XML?

Meskipun sama-sama *markup language*, HTML dan XML memiliki perbedaan sebagai berikut:

HTML                                        |   XML                                             |
--------------------------------------------|---------------------------------------------------|
|   HyperText Markup Language               |   eXtensible Markup Language                      |
|   HTML bersifat statis                    |    XML bersifat dinamis                           |
|   Tags sudah disediakan                   |   Tags dibuat sendiri                             |
|   Tidak *case sensitive*                  |   *Case sensitive*                                |
|   Tag digunakan untuk menampilkan data    |   Tag digunakan untuk mendeskripsikan data        |
|   HTML digunakan untuk menampilkan data   |  XML digunakan untuk menyimpan data atau membawa data dari database |
