import 'package:flutter/material.dart';

import '../../widgets/_widgets.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Container(
        color: Colors.blueGrey[200],
      ),
    );
  }
}
