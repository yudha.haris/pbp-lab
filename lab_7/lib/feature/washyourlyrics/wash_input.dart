import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/widgets/_widgets.dart';

class WashInput extends StatefulWidget {
  @override
  _WashInputState createState() => _WashInputState();
}

class _WashInputState extends State<WashInput> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Container(
        color: Colors.purple[100],
      ),
    );
  }
}
