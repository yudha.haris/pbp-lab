import 'package:flutter/material.dart';
import 'package:lab_7/widgets/_widgets.dart';

class WashOutput extends StatefulWidget {
  @override
  _WashOutputState createState() => _WashOutputState();
}

class _WashOutputState extends State<WashOutput> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Container(),
    );
  }
}
