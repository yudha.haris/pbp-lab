import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/widgets/_widgets.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: MediaQuery.of(context).size / 12,
        child: CustomAppBar(),
      ),
      drawer: MainDrawer(),
      body: Container(),
    );
  }
}
