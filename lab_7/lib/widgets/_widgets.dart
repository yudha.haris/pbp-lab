import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_7/core/constants/custom_size.dart';
import 'package:lab_7/core/states/note_state.dart';
import 'package:lab_7/feature/commentpengalaman/_screens.dart';
import 'package:lab_7/feature/obat/list_obat.dart';
import 'package:lab_7/feature/profile/profile.dart';
import 'package:lab_7/feature/register_login/login.dart';
import 'package:lab_7/feature/register_login/register.dart';
import 'package:lab_7/feature/sharepengalaman/stories.dart';
import 'package:lab_7/feature/washyourlyrics/wash_input.dart';
import 'package:provider/provider.dart';

part 'custom_appbar.dart';
part 'custom_listview.dart';
part 'main_drawer.dart';
part 'popup.dart';
part 'sidebar_menu_button.dart';
