part of '_widgets.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: CustomSize.maxWidth(context) / 1.5,
      color: Colors.indigo[50],
      height: CustomSize.maxHeight(context),
      child: SafeArea(
        child: Column(
          children: [
            InkWell(
              onTap: () {
                Navigator.push(context, CupertinoPageRoute(builder: (context) => Profile()));
              },
              child: Container(
                width: CustomSize.maxWidth(context) / 1.5,
                child: Icon(
                  Icons.account_circle_sharp,
                  size: CustomSize.maxWidth(context) / 2,
                  color: Colors.blueGrey,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context, CupertinoPageRoute(builder: (context) => CommentPengalaman()));
              },
              child: Container(
                width: CustomSize.maxWidth(context) / 1.5,
                padding: EdgeInsets.only(top: 5, bottom: 20),
                alignment: Alignment.center,
                child: Text(
                  'Welcome Stranger!',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25),
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                    color: Theme.of(context).bottomAppBarColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10), topRight: Radius.circular(10))),
                child: Column(
                  children: [
                    SideBarMenuButton(
                        'Informasi Obat',
                        Icon(
                          Icons.medical_services,
                          color: Colors.white,
                        )),
                    SideBarMenuButton(
                        'Pengalaman',
                        Icon(
                          Icons.edit,
                          color: Colors.white,
                        )),
                    SideBarMenuButton(
                        'WashYourLyrics',
                        Icon(
                          Icons.music_note,
                          color: Colors.white,
                        )),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context, CupertinoPageRoute(builder: (context) => Register()));
                        },
                        child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            child: Text('Create an Account!')),
                        style: ElevatedButton.styleFrom(
                            primary: Theme.of(context).cardColor,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
                      ),
                    ),
                    Container(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Do you have account?',
                        ),
                        InkWell(
                            onTap: () {
                              Navigator.push(
                                  context, CupertinoPageRoute(builder: (context) => Login()));
                            },
                            child: Text(' Log in', style: TextStyle(color: Colors.white)))
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
