class Note {
  String sender;
  String body;
  Note({required this.sender, required this.body});

  String getSender() => sender;
  String getBody() => body;
}
