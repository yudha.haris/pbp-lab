from django import forms
from django.forms import widgets
from .models import Note

# GeeksForGeeks
class DateInput(forms.DateInput):
    input_type = "date"

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        widgets = {"dob":DateInput()}
