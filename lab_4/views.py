from django.shortcuts import render
from .models import Note
from .forms import NoteForm
from django.http.response import HttpResponseRedirect

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)

    # validasi apakah form valid dan request methodnya POST
    if (form.is_valid() and request.method=="POST"):
        form.save()

        # kembali ke halaman /lab_4 setelah berhasil save
        return HttpResponseRedirect('/lab-4')

    return render(request, 'lab4_form.html', {'form':form})

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
